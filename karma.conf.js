module.exports = function (config) {
   config.set({
      basePath: '',
      frameworks: ['jasmine'],
      files: [

         'bower_components/angular/angular.js',
         'bower_components/angular-mocks/angular-mocks.js',
         'test/**/*.js',
         'app/**/*.js',
         'data.js'
      ],

      // list of files to exclude
      exclude: [],
      preprocessors: {},
      reporters: ['progress'],
      port: 9876,
      colors: true,
      logLevel: config.LOG_INFO,
      autoWatch: false,
      browsers: ['Chrome'],
      singleRun: true
   });
};
