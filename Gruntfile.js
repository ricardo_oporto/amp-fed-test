module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        sassFiles: 'app/**/*.scss',

        sass: {
            dist: {
                files: {
                    'build/site.css': 'app/site.scss'
                }
            }
        },

        watch: {
            sass: {
                tasks: ['sass'],
                files: 'app/**/*.scss'
            }

        },

       serve: {
          options: {
             port: 9000
          }
       },

       karma: {
          unit: {
             configFile: 'karma.conf.js'
          }
       }
    });


    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-serve');
    grunt.loadNpmTasks('grunt-karma');

    grunt.registerTask('dev', ['default', 'watch']);
    grunt.registerTask('default', ['sass', 'serve']);
    grunt.registerTask('test', ['karma']);

};