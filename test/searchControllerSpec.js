/**
 * Created by ricardo.oporto on 24/03/16.
 */
'use strict';
describe('Controller: searchController', function() {

   var  scope, $controller,  $httpBackend, $filter;

   var data = [
      {
         'id': '1',
         'firstName': 'Sean',
         'lastName': 'Kerr',
         'picture': 'img/sean.jpg',
         'Title': 'Senior Developer'
      },
      {
         'id': '2',
         'firstName': 'Yaw',
         'lastName': 'Ly',
         'picture': 'img/yaw.jpg',
         'Title': 'AEM Magician'
      },
      {
         'id': '3',
         'firstName': 'Lucy',
         'lastName': 'Hehir',
         'picture': 'img/lucy.jpg',
         'Title': 'Scrum master'
      },
      {
         'id': '4',
         'firstName': 'Rory',
         'lastName': 'Elrick',
         'picture': 'img/rory.jpg',
         'Title': 'AEM Guru'
      },
      {
         'id': '5',
         'firstName': 'Eric',
         'lastName': 'Kwok',
         'picture': 'img/eric.jpg',
         'Title': 'Technical Lead'
      },
      {
         'id': '6',
         'firstName': 'Hayley',
         'lastName': 'Crimmins',
         'picture': 'img/hayley.jpg',
         'Title': 'Dev Manager'
      }
   ];
   beforeEach(module('amp-test'));

   beforeEach(inject(function (_$controller_, _$rootScope_, _$httpBackend_, _$filter_ ) {

      scope = _$rootScope_.$new();
      $controller = _$controller_;
      $httpBackend = _$httpBackend_;
      $filter = _$filter_;

      $controller('searchController', {'$scope' : scope});

   }));

   it('should load items from service', function() {

      $httpBackend.when('GET', 'data.json')
          .respond(data);

      $httpBackend.flush();

      expect(scope.items.length).toEqual(6);

   });

   it('should filter data based on item name', function () {

      var result;

      result = $filter('filter')(data, {firstName:'Eric'});

      expect(result.length).toEqual(1);

   });

});
