/**
 * Created by ricardo.oporto on 24/03/16.
 */
'use strict';
angular.module('amp-test').controller('searchController',
    [ '$scope', 'dataProvider',
       function ($scope, dataProvider) {

          $scope.items = null;

          dataProvider.getItems().then(function ongetItems(items) {
             $scope.items = items.data;
          });


       }
    ]
);
