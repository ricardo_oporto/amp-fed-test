/**
 * Created by ricardo.oporto on 24/03/16.
 */
'use strict';
angular.module('amp-test').factory("dataProvider", ['$http', function ($http) {


   var _getItems = function(){

      return $http.get('data.json');

   };

   return {
      getItems: _getItems
   }
}]);
